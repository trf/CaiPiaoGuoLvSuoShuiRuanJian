package SecFaultTol;

public class UtilArithmetic
{
    /**
     * 万百和，传万位和百位，返回int类型,开关（0，0）*/
    public static int tenThdSumHund(int tenThousand ,int hundred)
    {
        int sum=tenThousand+hundred;
        if(sum>=10)
        {
            sum=sum-10;
        }

        return sum;
    }
    /**
     * 万百差，传万位和百位，返回int类型,开关（0，1）*/
    public static int tenThdSubHund(int tenThousand ,int hundred)
    {
        int sub=tenThousand-hundred;
        if(sub<0)
        {
            sub=sub+10;
        }
        return sub;

    }
    /**
     * 万千和，传万位和千位，返回int类型,开关（1，0）*/
    public static int tenThdSumThd(int tenThousand,int thousand)
    {
        int sum=tenThousand+thousand;
        if(sum>=10)
        {
            sum=sum-10;
        }


        return sum;
    }
    /**
     * 万千差，传万位和千位，返回int类型,开关（1，1）*/
    public static int tenThdSubThd(int tenThousand,int thousand)
    {
        int sub=tenThousand-thousand;
        if(sub<0)
        {
            sub=sub+10;
        }
        return sub;
    }
    /**
     * 千百和，传千位和百位，返回int类型,开关（2，0）*/
    public static int thdSumHundred(int thousand,int hundred)
    {

        int sum=thousand+hundred;
        if(sum>=10)
        {
            sum=sum-10;
        }
        return sum;
    }
    /**
     * 千百差，传千位和百位，返回int类型,开关（2，1）*/
    public static int thdSubHundred(int thousand,int hundred)
    {

        int sub=thousand-hundred;
        if(sub<0){
            sub=sub+10;
        }
        return sub;
    }
}
