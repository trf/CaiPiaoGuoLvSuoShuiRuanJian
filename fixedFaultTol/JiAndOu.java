package fixedFaultTol;

import java.util.ArrayList;
import java.util.Iterator;

public class JiAndOu {
	
		private  static int []NUMBER=new int[3];
	    //判断奇偶的方法	
		private  static int JO(int m){
			if(m%2==0){return 0;}
			else{return 1;}
		}
		//将百位、十位、个位存入一个新数组
		private  static int[] JandO(int n){
			NUMBER[0] =JO(n/100);
			NUMBER[1] =JO((n%100)/10);
			NUMBER[2] =JO((n%100)%10);
			return NUMBER;
			
			
		}
		//ooo:偶偶偶
		public  static ArrayList<Integer>  ooo (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==1||array[1]==1||array[2]==1){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
	    
	  }
		//ooj:偶偶奇
		public  static ArrayList<Integer>  ooj (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==1||array[1]==1||array[2]==0){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
	    
	  }
		//ojo:偶奇偶
		public  static ArrayList<Integer>  ojo (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==1||array[1]==0||array[2]==1){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
	    
	  }
		//ojj:偶奇奇
		public  static ArrayList<Integer>  ojj (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==1||array[1]==0||array[2]==0){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
	    
	  }
		//joo:奇偶偶
		public  static ArrayList<Integer>  joo (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==0||array[1]==1||array[2]==1){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
	    
	  }
		//joj:奇偶奇
		public  static ArrayList<Integer>  joj (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==0||array[1]==1||array[2]==0){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
	    
	  }
		//jjo:奇奇偶
		public  static ArrayList<Integer>  jjo (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==0||array[1]==0||array[2]==1){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
	    
	  }
		//jjj:奇奇奇
		public  static ArrayList<Integer>  jjj (ArrayList<Integer> sn)
		{
			
				
		Iterator <Integer> iter=sn.iterator();
		
		//过滤
		while (iter.hasNext())
		{
			int n=iter.next();
			int [] array=JandO(n);
			if(array[0]==0||array[1]==0||array[2]==0){
				iter.remove();//将不满足条件的数值过滤
			}
		}
		return sn;
		}
	  
}
